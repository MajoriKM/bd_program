﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Boulder_Dash
{
    class Dirt : Tile
    {

        // ------------------
        // Behaviour
        // ------------------

        public Dirt(Texture2D newTexture)
            : base(newTexture)
        {
        }
        

    }
}
