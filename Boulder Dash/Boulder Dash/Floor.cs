﻿using Microsoft.Xna.Framework.Graphics;

namespace Boulder_Dash
{
    class Floor : Tile
    {

        // ------------------
        // Behaviour
        // ------------------
        public Floor(Texture2D newTexture)
            : base(newTexture)
        {
        }
        // ------------------

    }
}
