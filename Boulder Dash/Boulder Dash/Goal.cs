﻿using Microsoft.Xna.Framework.Graphics;

namespace Boulder_Dash
{
    class Goal : Tile
    {

        private Level ourLevel;
        

        private Texture2D closedDoorTexture;
        private Texture2D openDoorTexture;

        public Goal(Texture2D newClosedDoorTexture, Texture2D newOpenDoorTexture, Level newLevel)
            : base(newClosedDoorTexture)
        {
            ourLevel = newLevel;
            closedDoorTexture = newClosedDoorTexture;
            openDoorTexture = newOpenDoorTexture;
        }

        public void OpenDoor()
        {
            if (ourLevel.GetScore() == 5)
            {
                // changes the Doors sprite so that it looks like it opened
                texture = openDoorTexture;
            }
            else
            {
                texture = closedDoorTexture;
            }
        }
    }
}
