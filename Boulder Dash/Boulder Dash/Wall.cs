﻿using Microsoft.Xna.Framework.Graphics;

namespace Boulder_Dash
{
    class Wall : Tile
    {

        // ------------------
        // Behaviour
        // ------------------
        public Wall(Texture2D newTexture)
            : base(newTexture)
        {
        }
        // ------------------

    }
}
