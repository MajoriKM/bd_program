﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace Boulder_Dash
{
    class WinScreen : Screen
    {

        //--------------------------
        // Data
        //--------------------------
        private Text gameName;
        private Text startPrompt;
        private Game1 game;

        //--------------------------
        // Behaviour
        //--------------------------

        public WinScreen(Game1 endGame)
        {
            game = endGame;
        }

        public void LoadContent(ContentManager content, GraphicsDevice graphics)
        {
            SpriteFont titleFont = content.Load<SpriteFont>("fonts/largeFont");
            SpriteFont smallFont = content.Load<SpriteFont>("fonts/mainFont");

            gameName = new Text(titleFont);
            gameName.SetTextString("CONGRATULATIONS");
            gameName.SetAlignment(Text.Alignment.CENTRE);
            gameName.SetColor(Color.White);
            gameName.SetPosition(new Vector2(graphics.Viewport.Bounds.Width / 2, 100));

            startPrompt = new Text(smallFont);
            startPrompt.SetTextString("[Press Esc to Quit the Game]");
            startPrompt.SetAlignment(Text.Alignment.CENTRE);
            startPrompt.SetColor(Color.White);
            startPrompt.SetPosition(new Vector2(graphics.Viewport.Bounds.Width / 2, 200));
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            gameName.Draw(spriteBatch);
            startPrompt.Draw(spriteBatch);
        }
        // ------------------
        public override void Update(GameTime gameTime)
        {
            
        }

    }
}
